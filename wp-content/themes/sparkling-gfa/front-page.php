<?php

    if ( get_option( 'show_on_front' ) == 'posts' ) {
        get_template_part( 'index' );
    } elseif ( 'page' == get_option( 'show_on_front' ) ) {

get_header(); ?>
</div>
<div id="content" class="site-content container">
	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

			<div id="info-gallery">
				<div class="info-gallery-element">
					<a href="http://google.com" class="info-gal-h2">Academics</a>
					<img src="http://cdn.impressivewebs.com/2011-11/greece001.jpg" alt="" width="400" height="260">
				</div>
				<div class="info-gallery-element center">
					<a href="http://google.com" class="info-gal-h2">Mission</a>
					<img src="http://cdn.impressivewebs.com/2011-11/greece002.jpg" alt="" width="400" height="260">
				</div>
				<div class="info-gallery-element">
					<a href="http://google.com" class="info-gal-h2">Faculty</a>
					<img src="http://cdn.impressivewebs.com/2011-11/greece003.jpg" alt="" width="400" height="260">
				</div>
			</div>
			<div id="widget-gallery">

				<div class="widget-element">
					<?php dynamic_sidebar( 'home-widget-1' ); ?>
				</div>
				<div class="widget-element">
					<?php dynamic_sidebar( 'home-widget-2' ); ?>
				</div>
				<div class="widget-element">
					<?php dynamic_sidebar( 'home-widget-3' ); ?>
				</div>
			</div>

			<div class="post-inner-content">


				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content(); ?>
							<?php
								wp_link_pages( array(
									'before' => '<div class="page-links">' . __( 'Pages:', 'sparkling' ),
									'after'  => '</div>',
								) );
							?>
						</div><!-- .entry-content -->
						<?php edit_post_link( __( 'Edit', 'sparkling' ), '<footer class="entry-meta"><i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span></footer>' ); ?>
					</article><!-- #post-## -->


					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() ) :
							comments_template();
						endif;
					?>

				<?php endwhile; // end of the loop. ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php
	get_footer();
}
?>