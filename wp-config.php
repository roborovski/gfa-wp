<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'root_wordpress-trunk');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dRui)/<H+P-S:?)]++BTD`wvm&gU|Zb5}._c4iU*b5PKHNC[T+&+.YYN%-e*]YR/');
define('SECURE_AUTH_KEY',  '0Xb 6~!g&][2B3-W)Ru?2F|L^3-BFr+E-fZqe<#~fG!@p{B-&0{lk$~4G5gQw]bp');
define('LOGGED_IN_KEY',    'B%K;55 l`pd%qIsB@F5xl6:/ze?|Vq X,XrG%#<^KM@[3L]T>d{7gZ$P5^J;JcPk');
define('NONCE_KEY',        '-sx_+PZp8f--sXtO~-g3WVPoy]ak(*V^4N><(gtiL4u,)hI*AY_ps+]?O`m(oZ&|');
define('AUTH_SALT',        'x%G*lpWO+L3zlMKEaL$K2jbwd+m1KN-SayG-hY)S&3]ZRX7)d@FyJ Y<Iv&ttFja');
define('SECURE_AUTH_SALT', '?[/+IQ/,T`W}A{y0D Bz0|v2d,53=Y%?O.PT qc|%pWG6I2J5v3EU|?=S>aXX}SY');
define('LOGGED_IN_SALT',   'u3Ip=al/V1t+1tOHI~hhFJQC/;2S;JjObRA-$J-eMc^H3G.Yb(-&Gnxc/*N+dGW|');
define('NONCE_SALT',       '!sy`2$rEX7rms#RI[)b D#Pram?0|^iZldE-XLwJC!c=}/[1D+Ou6Swk|RHCWjU3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
