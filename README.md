This is the redesign for the Governor French Academy website I was commissioned to develop in the summer of 2014. I modified the 'Sparkling' theme by colorlib (under the GPL) although the full development site may be of use in setting parts of the theme up. I'd advise simply copying the entire site to the live server to avoid any issues.

In the root directory there is a file named root-wordpress-trunk.sql, this is an export of the test database.

Here are the mySQL settings for the wordpress installation: 


/** The name of the database for WordPress */
define('DB_NAME', 'root_wordpress-trunk');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


Color Scheme:
    Light Green: 81d742
    Dark Green: 0d703d
    Dark Grey: 363636
    Light Grey: dadada


If you have any questions or concerns, please feel free to email me at 
